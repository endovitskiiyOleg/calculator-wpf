﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Calculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string leftop = "";
        string operation = "";
        string rightop = "";
        public MainWindow()
        {
            InitializeComponent();
            foreach (UIElement c in LayoutRoot.Children)
            {
                if (c is Button)
                {
                    ((Button)c).Click += Button_Click;
                }
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //Получаем текст кнопки
            string s = (string)((Button)e.OriginalSource).Content;
            if (s != "Изменить тему")
            {
                textBlock.Text += s;
                double num;
                //Пытаемся преобразовать его в число
                bool result = Double.TryParse(s, out num);
                if (result == true)
                {
                    if (operation == "")
                    {
                        leftop += s;
                    }
                    else
                    {
                        rightop += s;
                    }
                }
                else
                {
                    //Если равно, то выводим результат операции
                    if (s == "=")
                    {
                        Update_RightOp();
                        textBlock.Text += rightop;
                        operation = "";
                    }
                    else if (s == "C")
                    {
                        leftop = "";
                        rightop = "";
                        operation = "";
                        textBlock.Text = "";
                    }
                    //Получаем операцию
                    else
                    {
                        if (rightop != "")
                        {
                            Update_RightOp();
                            leftop = rightop;
                            rightop = "";
                        }
                        operation = s;
                    }
                }
            }
        }
        private void Update_RightOp()
        {
            double num1 = Double.Parse(leftop);
            double num2 = Double.Parse(rightop);
            switch (operation)
            {
                case "+":
                    rightop = (num1 + num2).ToString();
                    break;
                case "-":
                    rightop = (num1 - num2).ToString();
                    break;
                case "*":
                    rightop = (num1 * num2).ToString();
                    break;
                case "/":
                    rightop = (num1 / num2).ToString();
                    break;
                default: break;
            }
        }
        private void Click(object sender, RoutedEventArgs e)
        {
                if (Button1.Background == Brushes.Black)
                {
                    Button1.Background = Brushes.White;
                    Button1.Foreground = Brushes.Black;
                    Button2.Background = Brushes.White;
                    Button2.Foreground = Brushes.Black;
                    Button3.Background = Brushes.White;
                    Button3.Foreground = Brushes.Black;
                    Button4.Background = Brushes.White;
                    Button4.Foreground = Brushes.Black;
                    Button5.Background = Brushes.White;
                    Button5.Foreground = Brushes.Black;
                    Button6.Background = Brushes.White;
                    Button6.Foreground = Brushes.Black;
                    Button7.Background = Brushes.White;
                    Button7.Foreground = Brushes.Black;
                    Button8.Background = Brushes.White;
                    Button8.Foreground = Brushes.Black;
                    Button9.Background = Brushes.White;
                    Button9.Foreground = Brushes.Black;
                    Button10.Background = Brushes.White;
                    Button10.Foreground = Brushes.Black;
                    Button11.Background = Brushes.White;
                    Button11.Foreground = Brushes.Black;
                    Button12.Background = Brushes.White;
                    Button12.Foreground = Brushes.Black;
                    Button13.Background = Brushes.White;
                    Button13.Foreground = Brushes.Black;
                    Button14.Background = Brushes.White;
                    Button14.Foreground = Brushes.Black;
                    Button15.Background = Brushes.White;
                    Button15.Foreground = Brushes.Black;
                    Button16.Background = Brushes.White;
                    Button16.Foreground = Brushes.Black;
                    Button17.Background = Brushes.White;
                    Button17.Foreground = Brushes.Black;
                }
                else
                {
                    Button1.Background = Brushes.Black;
                    Button1.Foreground = Brushes.White;
                    Button2.Background = Brushes.Black;
                    Button2.Foreground = Brushes.White;
                    Button3.Background = Brushes.Black;
                    Button3.Foreground = Brushes.White;
                    Button4.Background = Brushes.Black;
                    Button4.Foreground = Brushes.White;
                    Button5.Background = Brushes.Black;
                    Button5.Foreground = Brushes.White;
                    Button6.Background = Brushes.Black;
                    Button6.Foreground = Brushes.White;
                    Button7.Background = Brushes.Black;
                    Button7.Foreground = Brushes.White;
                    Button8.Background = Brushes.Black;
                    Button8.Foreground = Brushes.White;
                    Button9.Background = Brushes.Black;
                    Button9.Foreground = Brushes.White;
                    Button10.Background = Brushes.Black;
                    Button10.Foreground = Brushes.White;
                    Button11.Background = Brushes.Black;
                    Button11.Foreground = Brushes.White;
                    Button12.Background = Brushes.Black;
                    Button12.Foreground = Brushes.White;
                    Button13.Background = Brushes.Black;
                    Button13.Foreground = Brushes.White;
                    Button14.Background = Brushes.Black;
                    Button14.Foreground = Brushes.White;
                    Button15.Background = Brushes.Black;
                    Button15.Foreground = Brushes.White;
                    Button16.Background = Brushes.Black;
                    Button16.Foreground = Brushes.White;
                    Button17.Background = Brushes.Black;
                    Button17.Foreground = Brushes.White;
                }
        }
    }
}
